

# Pyx.  Experiments with Python.


# Known syntax to try out.
#   ... then ...
# Binary search with imperative syntax, under test.
# Binary search with idiomatic syntax, with the same tests.
#   ... then maybe ... 
# Alternative idiomatic binary search implemns, if I feel like it.
# Some basic data structure with idiomatic syntax.
# Some basic design pattern with idiomatic syntax.

# IDE downgrade: Have to use Ctrl+K++C to comment, Ctrl+K++U to uncomment.



# Intro text.  Printing syntax.
print()
print("Pyx.  Experiments with Python.")
print()


# Conventions here: 
#   x, y, n = value, variable 
#   e = expression 
#   p = parameters / args 
#   i = interval (often "stride") 
#   other letters = values, variables 



# Function / method syntax: `def [name]: [indented contents]`.
def tallprint(text):
    print()
    print(text)
    print()


# Function / method call: same as in other languages.
tallprint("This line was printed using my `tallprint()` function / method.")


# F(p) syntax: Same as F(), parameters same as other languages, minus types.
def padprint(text, before, after):
    # Loop / condition syntax: `expression:` and the body indented.
    for x in range(before):
        print()
    print(text)
    # Numbered-iteration loop: `for x in range(p):`
    for x in range(after):
        print()


print()


# Variables are just assigned to create them (aside from some constructs like `for`).
x = 0;

# range():  
#   range(n) => 0 to n -1  
#   range(x, n) => x to n -1  
#   range(x, n, i) => every i'th of x to n -1 

tallprint("r = range(6)\nfor x in r: print(x)")

r = range(6)
for x in r: print(x)

tallprint("r = range(2, 6)\nfor x in r: print(x)")

r = range(2, 6)
for x in r: print(x)


tallprint("r = range(2, 6, 2)\nfor x in r: print(x)")

r = range(2, 6, 2)
for x in r: print(x)


tallprint("r = range(2, 7, 2)\nfor x in r: print(x)");

# To get an inclusive end, you have to go one higher, yuck.
r = range(2, 7, 2)
for x in r: print(x)


# An "f string" is just a format string using modern 
# { e } syntax, but with `f` as the preceding sign.
f = "\"f-string\""
padprint(f"Printed again, using { f } syntax in a call to my `padprint()`.", 2, 2)



# yield e           =>      generator (call to send()) 
# lambda p: e       =>      creates lambda to return result of e 
# x if n else y     =>      ternary, true-return before condition 
# x or y            =>      x || y 
# x and y           =>      x && y 
# not x             =>      !x 
# x in y            =>      y.includes(x) 
# x not in y        =>      !y.includes(x) 
# x is y            =>      x === y 
# x is not y        =>      x !== y 
# x > y,            =>      greater than, strict superset 
# x >= y            =>      greater than or equal to, superset 
# x < y             =>      less than, strict subset 
# x <= y            =>      less than or equal to, subset 
# x == y            =>      normal 
# x != y            =>      normal 

# continuing at middle of page 13 in reftext... 



